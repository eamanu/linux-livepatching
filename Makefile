KERNEL_VERSION		?= $(shell uname -r)
KVERSION_MAJOR_MINOR	:= $(shell uname -r | awk -F. '{print $$1"."$$2}')
KERNEL_DIR		?= /lib/modules/$(KERNEL_VERSION)/build
KPATCH-BUILD		= kpatch-build
KERNEL_SOURCE 		= /usr/src/linux-source-$(KVERSION_MAJOR_MINOR).tar.xz
KERNEL_CONFIG           = config-$(KERNEL_VERSION)
KERNEL_CONFIG_PATH 	= /boot/$(KERNEL_CONFIG)
KERNEL_DEBUG_PATH	= /usr/lib/debug/vmlinux-$(KERNEL_VERSION)
DEBUG_FLAGS 		= --skip-compiler-check --debug
FLAGS			:= --j=$(shell nproc) --skip-compiler-check


build:
	tar xavf $(KERNEL_SOURCE) -C $(PWD)
	cp $(KERNEL_CONFIG_PATH) $(PWD)
	sed -i 's/CONFIG_X86_KERNEL_IBT=y/CONFIG_X86_KERNEL_IBT=n/g' $(KERNEL_CONFIG)
	# This is a workaround for now
	cd linux-source-$(KVERSION_MAJOR_MINOR)/; make olddefconfig; cd ../
	$(KPATCH-BUILD) --vmlinux $(KERNEL_DEBUG_PATH) -s linux-source-$(KVERSION_MAJOR_MINOR)/ -c $(PWD)/linux-source-$(KVERSION_MAJOR_MINOR)/.config $(shell find . -type f -name "*.patch") $(FLAGS)

clean:
	rm linux-source-$(KVERSION_MAJOR_MINOR)/ -rf
	rm $(KERNEL_CONFIG) -f
load:
	insmod $(PATCHNAME).ko

install:
	# mkdir -p $(CURDIR)/debian/tmp/usr/lib/modules/$(KERNELVERSION)/$(PATCHNAME)
	# install -m 0755 -o root -g root $(PATCHNAME).ko $(CURDIR)/debian/tmp/usr/lib/modules/$(KERNELVERSION)/$(PATCHNAME)
	kpatch $(shell find . -type f -name "*.ko")
